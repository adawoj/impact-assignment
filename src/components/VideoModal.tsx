import styled from 'styled-components';
import { useState } from 'react';
import { Author, Category } from '../interfaces';
import { saveVideo } from '../services/videos';
import { Button } from './Button';

type VideoModalProps = {
  setShow: Function;
  categories: Category[];
  authors: Author[];
  refreshVideos: Function;
  currAuthorId?: number;
  currVideoId?: number;
};

const givenName = (authors: Author[], currAuthorId: number, currVideoId: number) => {
  return authors.find(({ id }) => id === currAuthorId)?.videos?.find(({ id }) => id === currVideoId)?.name;
};
const givenCats = (authors: Author[], currAuthorId: number, currVideoId: number) => {
  return authors
    .find(({ id }) => id === currAuthorId)
    ?.videos?.find(({ id }) => id === currVideoId)
    ?.catIds.map(String);
};

export const VideoModal = ({ setShow, categories, authors, refreshVideos, currAuthorId, currVideoId }: VideoModalProps) => {
  const isVideoEdited = !!(currAuthorId && currVideoId);
  const initialVideoName = isVideoEdited ? givenName(authors, currAuthorId, currVideoId) : '';
  const initialAuthor = isVideoEdited ? authors.find(({ id }) => id === currAuthorId) : undefined;
  const initialCats = isVideoEdited ? givenCats(authors, currAuthorId, currVideoId) : [];

  const [catIds, setCatIds] = useState<string[] | undefined>(initialCats);
  const [author, setAuthor] = useState<Author | undefined>(initialAuthor);
  const [videoName, setVideoName] = useState<string | undefined>(initialVideoName);

  const onSelectCategory = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const values = Array.from(e.target.selectedOptions).map((opt) => opt.value);
    setCatIds(values);
  };

  const onSelectAuthor = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedAuthor = authors.find((a) => a.id === parseInt(e.target.value));
    setAuthor(selectedAuthor);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    saveVideo(videoName as string, author as Author, catIds as string[], currVideoId as number).then(() => refreshVideos());
  };

  return (
    <Wrapper>
      <Modal>
        <ModalClose type="button" onClick={() => setShow(false)}>
          X
        </ModalClose>
        <ModalBody>
          <h2>{isVideoEdited ? 'Edit video' : 'Add video'}</h2>
          <form onSubmit={(e) => handleSubmit(e)}>
            <Label htmlFor="videoName">Name</Label>
            <input
              required
              id="videoName"
              type="text"
              value={videoName}
              placeholder="e.g. Moulin Rouge!"
              onChange={(e) => setVideoName(e.target.value)}
            />
            <Label htmlFor="author">Author</Label>
            <select
              required
              id="author"
              name="author"
              value={author?.id || ''}
              onChange={(e) => onSelectAuthor(e)}
              disabled={isVideoEdited}>
              <option key="empty" value="" disabled>
                Choose author
              </option>
              {authors.map((aut) => (
                <option key={aut.id} value={aut.id}>
                  {aut.name}
                </option>
              ))}
            </select>
            <Label htmlFor="categories">Categories</Label>
            <Tip>Hold down the Ctrl (Windows) or Command (Mac) button to select multiple options.</Tip>
            <select required multiple id="category" name="category" value={catIds} onChange={(e) => onSelectCategory(e)}>
              {categories.map((cat) => (
                <option key={cat.id} value={cat.id}>
                  {cat.name}
                </option>
              ))}
            </select>
            <SubmitButton primary type="submit">
              Save
            </SubmitButton>
          </form>
        </ModalBody>
      </Modal>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 98;
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.3);
`;

const Modal = styled.div`
  position: relative;
  z-index: 99;
  width: 100%;
  max-width: 600px;
  max-height: 100%;
  margin: 0 auto;
`;

const ModalClose = styled.button`
  position: absolute;
  top: -20px;
  right: 0;
  padding: 5px;
  border: 0;
  background: none;
  color: white;
  cursor: pointer;
`;
const ModalBody = styled.div`
  padding: 15px;
  border-radius: 4px;
  background-color: white;
`;
const Label = styled.label`
  margin-top: 20px;
  margin-bottom: 5px;
  display: block;
`;
const Tip = styled.p`
  font-size: 10px;
  padding: 0;
`;
const SubmitButton = styled(Button)`
  display: block;
  margin-left: auto;
  margin-right: 0;
`;
