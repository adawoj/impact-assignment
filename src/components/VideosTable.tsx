import type { ProcessedVideo } from '../interfaces';

import styled from 'styled-components';
import { Button } from './Button';
import { deleteVideo } from '../services/videos';

type VideosTableProps = {
  videos: ProcessedVideo[];
  refreshVideos: Function;
  openModal: Function;
};

export const VideosTable = ({ videos, refreshVideos, openModal }: VideosTableProps) => {
  const removeVideo = (authorId: number, id: number, videoName: string) => {
    window.confirm(`Are you sure you want to delete "${videoName}" video?`) &&
      deleteVideo(authorId, id).then(() => {
        refreshVideos();
      });
  };

  return (
    <Wrapper>
      <Table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Author</th>
            <th>Categories</th>
            <th>Options</th>
          </tr>
        </thead>

        <tbody>
          {videos.map((video) => (
            <tr key={video.id}>
              <td>{video.name}</td>
              <td>{video.author}</td>
              <td>{video.categories.join(', ')}</td>
              <td>
                <Button primary onClick={() => openModal(video.authorId, video.id)}>
                  Edit
                </Button>
                <Button secondary onClick={() => removeVideo(video.authorId, video.id, video.name)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  overflow: auto;
`;

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;

  th,
  td {
    padding: 16px;
    border: 1px solid;
    text-align: left;
  }
`;
