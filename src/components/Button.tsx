import styled from 'styled-components';

export const Button = styled.button<{ primary?: boolean; secondary?: boolean }>`
  padding: 8px;
  border: none;
  border-radius: 4px;
  color: var(--white);
  cursor: pointer;

  ${({ primary }) =>
    primary &&
    `
    background: var(--green);
  `}

  ${({ secondary }) =>
    secondary &&
    `
    background: var(--red);
  `}
`;
