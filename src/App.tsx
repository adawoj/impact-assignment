import type { Author, Category, ProcessedVideo } from './interfaces';

import { useEffect, useState } from 'react';
import styled from 'styled-components';

import { getVideos } from './services/videos';
import { VideosTable } from './components/VideosTable';
import { Button } from './components/Button';
import { VideoModal } from './components/VideoModal';

export const App = () => {
  const [videos, setVideos] = useState<ProcessedVideo[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);
  const [authors, setAuthors] = useState<Author[]>([]);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [currAuthorId, setCurrAuthorId] = useState<number | undefined>(undefined);
  const [currVideoId, setCurrVideoId] = useState<number | undefined>(undefined);

  const refreshVideos = () => {
    getVideos().then(({ videos, authors, categories }) => {
      setVideos(videos);
      setAuthors(authors);
      setCategories(categories);
      setShowModal(false);
    });
  };

  const openModal = (authorId?: number, videoId?: number) => {
    setCurrVideoId(videoId);
    setCurrAuthorId(authorId);
    setShowModal(true);
  };

  const modalProps = {
    setShow: setShowModal,
    categories,
    authors,
    refreshVideos,
    currAuthorId: currAuthorId,
    currVideoId: currVideoId,
  };

  useEffect(() => {
    refreshVideos();
  }, []);

  return (
    <>
      {showModal && <VideoModal {...modalProps} />}
      <Header>
        Videos
        <Button primary onClick={() => openModal()}>
          Add video
        </Button>
      </Header>

      <Main>
        <VideosTable videos={videos} refreshVideos={refreshVideos} openModal={openModal} />
      </Main>
      <Footer>Impact Coding Assignment</Footer>
    </>
  );
};

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 16px;
  background: var(--dark-gray);
  color: var(--white);
`;

const Main = styled.main`
  padding: 0 16px;
`;

const Footer = styled.footer`
  margin-top: auto;
  padding: 16px;
  background: var(--light-gray);
`;
