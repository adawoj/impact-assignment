import type { ProcessedVideo, Author, Category } from '../interfaces';

import { getCategories } from './categories';
import { getAuthor, getAuthors } from './authors';

const headers = new Headers();
headers.append('Content-Type', 'application/json');

type VideosResponse = {
  videos: ProcessedVideo[];
  authors: Author[];
  categories: Category[];
};

export const getVideos = async (): Promise<VideosResponse> => {
  const [categories, authors] = await Promise.all([getCategories(), getAuthors()]);
  const videos = authors.reduce((acc: any, author: Author) => {
    return [
      ...acc,
      ...author.videos.map((video) => ({
        id: video.id,
        name: video.name,
        author: author.name,
        authorId: author.id,
        categories: video.catIds.map((catId) => categories.find((c) => c.id === catId)?.name),
      })),
    ];
  }, []);
  return { videos, authors, categories };
};

export const deleteVideo = async (authorId: number, videoId: number): Promise<Author> => {
  let author = await getAuthor(authorId);
  const authorVideos = author.videos.filter((v) => v.id !== videoId);
  const response = await fetch(`${process.env.REACT_APP_API}/authors/${authorId}`, {
    method: 'PATCH',
    headers: headers,
    body: JSON.stringify({ videos: authorVideos }),
  });

  return response.json();
};

export const saveVideo = async (videoName: string, author: Author, catIds: string[], currVideoId: number): Promise<Author> => {
  const catIdsNumbers = catIds.map(Number);
  const newVideo = {
    id: currVideoId || Date.now(),
    name: videoName,
    catIds: catIdsNumbers,
  };

  const newVideosList = currVideoId ? author.videos.map((v) => (v.id === newVideo.id ? newVideo : v)) : [...author.videos, newVideo];

  const response = await fetch(`${process.env.REACT_APP_API}/authors/${author.id}`, {
    method: 'PATCH',
    headers: headers,
    body: JSON.stringify({ videos: newVideosList }),
  });

  return response.json();
};
