# Impact Coding Assignment

## Uruchomienie projektu

Upewnij się, że masz Node w wymaganej wersji (sprawdź w pliku `.nvmrc`). Jeśli korzystasz z nvm, możesz zrobić `nvm use`.

Następnie:

```sh
npm install
npm run server
npm start (w osobnym terminalu)
```

## Wymagania

Głównym zadaniem jest dodanie funkcjonalności dla:

- wyświetlania listy filmów
- dodawania nowego filmu
- edycji filmu
- usuwania filmu

Moesz poszukać komentarzy oznaczonych "TODO" w kodzie.

Jak będzie to wyglądać zależy od Ciebie. Uznajmy, że designer jest na urlopie :)
Aplikacja nie musi być super skomplikowana, a wręcz przeciwnie - im prostsza i im mniej dodatkowych zaleności w `package.json`, tym lepiej. Możesz też napisać parę przykładowych testów.
